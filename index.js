const express = require("express");
require("dotenv").config();
const app = express();
const port = process.env.PORT;

app.get("/", (_, res) => res.send("<h1>T.Truong created this site</h1>"));

app.listen(port, () => console.log(`App listening on port ${port}!`));
