const axios = require("axios");

axios
  .get(
    "https://shopee.vn/api/v2/search_items/?by=relevancy&keyword=truyen%20tranh&limit=1&page_type=search"
  )
  .then(function(response) {
    // handle success
    console.log(response.data.items[0].name);
  })
  .catch(function(error) {
    // handle error
    console.log(error);
  })
  .finally(function() {
    // always executed
  });
